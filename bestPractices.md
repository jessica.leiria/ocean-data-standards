# 1. Initiatives
1. [ The Ocean Data Standards and Best Practices Project (ODSBP)](http://www.oceandatastandards.org/): The OBPS Vision is “a future where there are agreed and broadly adopted methods across ocean research, operations and applications”. The objective of the Project is to achieve broad agreement and commitment to adopt a number of standards and best practices related to ocean data management and exchange.

    1.1 [ResourcePool](http://www.oceandatastandards.org/resourcepool-mainmenu-7) ResourcePool includes references on Metadata, Date and Time, Lat/Lon/Alt, Country names, Platform instances, Platform types, Science Words, Instruments, Units, Projects, Institutions, Parameters, Taxa, Ontology Resources, General Resources, Quality Assurance, QC - Quality Control, QC - T and S Profiles, QC - Surface T and S, QC - Sea Level, QC - Surface Waves, QC - Currents and Quality Flags.

1. [Ocean Best Practices Systems](https://www.oceanbestpractices.org/)
The System has been built on the OceanBestPractices of IODE and the lead work of the AtlantOS Project Work Package 6.4. Best Practices Working Group that developed the System concept. It was adopted in June 2019 by the Intergovernmental Oceanographic Commission as an international project co-sponsored by the Global Ocean Observing System (GOOS) and the International Oceanographic Data and Information Exchange (IODE).

1. [National Environmental Monitoring Site Identification System](https://envirolink.govt.nz/assets/Envirolink/1729-HZLC137-National-environmental-monitoring-site-identification2.pdf) : Propose a candidate convention for site identifiers and discuss any supporting systems and procedures that may be required in New Zealand.

# Publication and reports

1. [Evolving and Sustaining Ocean Best Practices and Standards for the Next Decade](https://www.frontiersin.org/articles/10.3389/fmars.2019.00277/full) - Frontiers in Marine Science

1. [Evolving and Sustaining Ocean Best Practices Workshop III](https://repository.oceanbestpractices.org/bitstream/handle/11329/1273/OBP_Workshop%20III_Oostende_2019_Proceedings_FINAL_20200511.pdf?sequence=4&isAllowed=y)

# Quality Flag

1. [Recommendation for a Quality Flag Scheme](https://www.iode.org/index.php?option=com_oe&task=viewDocumentRecord&docID=10402)

# Vocabularies and standardized variable names

1. [Technology for SeaDataNet Controlled Vocabularies for describing Marine and Oceanographic Datasets](https://www.iode.org/index.php?option=com_oe&task=viewDocumentRecord&docID=25099)

1. [Standards and best practices (JCOMM)](https://www.jcomm.info/index.php?option=com_content&view=article&id=159&Itemid=23)