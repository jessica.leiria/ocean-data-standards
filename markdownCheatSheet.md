# Markdown Cheatsheet

## Headers
---
#### This is an H4
###### This is an H6

## Font Style
----
*This is a*
and _so is this_

*This is a*
and __so is this__

## Lists
----
1. Red
1. Green
1. Blue
    1. light
    1. dark
 


## Links
----

Text anchor:
<a id="anchor"></a>
[Go to anchor](#anchor)

Online link: [Link](https://sqlbak.com
"optional title")


## Text Style

Tables
----
|Left |Center|Right|
|:-----|:----:|----:|
|1 |A |C |
|2 |B |D |

Codes
----

```python
print('hello');
```