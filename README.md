# Ocean Data Standards

This project includes manuals, reports and all relevant documents related to standards on Oceanographic Data.
Topics include marine metadata, vocabularies, code lists and ontologies, quality control of physical oceanographic and use of flags.

It's continually being updated :)